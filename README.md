# Regolith

Regolith is a project similar in function to Hall of Mirrors; Regolith is designed to be a framework of scripts,
folders, and configuration files to ease automation of things like Minecraft Fabric, Forge, and PaperMC servers, akin
to Hall of Mirrors being a framework to ease automation of rsync-based mirroring.
