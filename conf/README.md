* adminUser

    * file containing user name regolith should use for administration (read-only to running server)

* mcUser

    * file containing user name regolith should use for minecraft server

* mcGroup

    * file containing group name regolith should use for minecraft server

* minecraft-*@.service

    * service files for systemd for individual server types (i.e. PaperMC)

    * service files in systemd will look like: `minecraft-paper@serverName`

* server configuration

    * fabric

        * folder for Fabric server configurations

    * paper

        * folder for PaperMC server configurations

    * purpur

        * folder for Purpur server configurations

    * server instance config example

```toml
[minecraft]
version = "1.18.1" # minecraft version
autoupdate = true # set whether or not to auto-update server.jar on server startup, boolean

[minecraft.memory]
min = "204m" # minimum amount of memory allocated to java
max = "2048m" # maximum amount of memory allocated to java

[minecraft.runtime]
bin = "/usr/lib/jvm/jdk-17.0.1+12/bin/java" # location of java executable
args = "-XX:+UseConcMarkSweepGC -XX:+CMSIncrementalMode -XX:-UseAdaptiveSizePolicy" # any extra args to pass to the java executable
```

        * server instance configs go into the folder named the type of server they're configuring. for example, if `LavCorps.toml` is configuring a Fabric instance, it should be in the `fabric` folder.

        * **no config items are optional**

        * all config items should end in .toml
