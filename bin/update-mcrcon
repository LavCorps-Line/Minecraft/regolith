#!/bin/env -S python3 -u
# Standard Library Imports #
import json
import os
import stat
import tarfile

# Third-Party Imports #
import requests

print("regolith mcrcon updater initializing...")

mainDir = os.path.normpath(os.environ["mainDir"])

rconPath = mainDir + "/bin/mcrcon"
rconPathOld = mainDir + "/bin/mcrcon.old"
rconPathGz = rconPath + ".tar.gz"

print("systemd mcrcon updater initialized!")

r = requests.get("https://api.github.com/repos/Tiiffi/mcrcon/releases/latest")
j = json.loads(r.content)
downloadURL = ""

for asset in j["assets"]:
    if "linux" in asset["name"] and "x86-64" in asset["name"]:
        downloadURL = asset["browser_download_url"]

if os.path.exists(rconPath):
    print("mcrcon already exists, backing up...")
    if os.path.exists(rconPathOld):
        print("Deleting old backup...")
        os.remove(rconPathOld)
    os.rename(rconPath, rconPathOld)

print("Downloading mcrcon...")
if os.path.exists(rconPathGz):
    os.remove(rconPathGz)
with open(rconPathGz, "wb") as outFile:
    outFile.write(requests.get(downloadURL).content)
print("Extracting mcrcon...")
with tarfile.open(rconPathGz) as rconGz:
    mcrconFile = None
    try:
        mcrconFile = rconGz.getmember("mcrcon")
        assert mcrconFile.isfile()
    except KeyError as KeEr:
        print("mcrcon is not inside the file we downloaded")
        print("this shouldn't happen - consider opening an issue")
        print("at https://gitlab.com/LavCorps-Line/Minecraft/regolith/-/issues/new")
        raise KeEr
    else:
        rcon = rconGz.extractfile(mcrconFile)
        with open(rconPath, "wb") as f:
            f.write(rcon.read())
        os.chmod(
            rconPath,
            stat.S_IRUSR + stat.S_IWUSR + stat.S_IXUSR + stat.S_IRGRP + stat.S_IXGRP + stat.S_IROTH + stat.S_IXOTH,
        )  # um...
if os.path.exists(rconPathGz):
    os.remove(rconPathGz)
print("mcrcon updated!")
