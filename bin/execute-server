#!/usr/bin/env -S python3 -u
# Standard Library Imports #
import asyncio
import os
import sys

# Third-Party Imports #
import toml
import uvloop

asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())

mainDir = None
mcInstance = None
javaPath = None
Xmx = None
Xms = None
args = None

try:
    mainDir = os.path.normpath(os.environ["mainDir"])
    mcInstance = os.environ["MC_INSTANCE"]
    serverType = os.environ["serverType"]
except KeyError as KeEr:
    print("We're missing environmental variables!")
    print("Are we being run from the proper script?")
    raise KeEr

try:
    with open(f"{mainDir}/conf/{serverType}/{mcInstance}.toml", "r") as f:
        cfg = toml.load(f)
        javaPath = cfg["minecraft"]["runtime"]["bin"]
        Xmx = cfg["minecraft"]["memory"]["max"]
        Xms = cfg["minecraft"]["memory"]["min"]
        args = cfg["minecraft"]["runtime"]["args"]
        if type(Xmx) == int or type(Xms) == int:
            print(
                "Xmx or Xms are an integer! They should be a string, the number including unit of measurement, i.e. 1024m for 1024 megabytes of ram."
            )
            raise ValueError("Java heap size is provided as an int when it should be a str!")
except FileNotFoundError as FiNoFoEr:
    print("We can't open the following configuration file:")
    print(f"{mainDir}/conf/{serverType}/{mcInstance}.toml")
    print("Please make sure the file exists!")
    raise FiNoFoEr


async def main():
    print(f"Running Server {mcInstance}...")
    os.chdir(f"{mainDir}/server/{serverType}/{mcInstance}")
    proc = await asyncio.create_subprocess_shell(
        f"{javaPath} -Xmx{Xmx} -Xms{Xms} {args} -jar server.jar nogui", stdout=sys.stdout, stderr=sys.stderr
    )
    await proc.wait()
    exCode = proc.returncode
    print(f"Server {mcInstance} exited with code {exCode}")


asyncio.run(main())
